import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'testString'
})
export class TestStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const prefix = args ;
    if ( prefix ) {
      return 'error !! : ' + value + ' / args : ' + prefix ;
    }else {
      return 'error !! : ' + value ;
    }
  }

}
