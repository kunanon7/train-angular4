import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class ProjectService {

  constructor(private http: Http) {
  }

  loadItem(project): Observable<any[]> {
    return this.http.get( environment.remoteAPI + '/api/v1/project')
    .map((res: Response) => {
      return res.json();
    })
    .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


}
