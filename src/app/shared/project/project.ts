export class Project {
  pkCode: string;
  projCode: string;
  projName: string;
  projImage: string;
  status: string;
}
