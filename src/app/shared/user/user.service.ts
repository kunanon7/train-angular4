import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class UserService {

  options: RequestOptions;

  constructor(private http: Http) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    this.options = new RequestOptions({ headers: headers });
  }

  login(user): Observable<any> {
    const bodyString = JSON.stringify(user);
    return this.http.post( environment.remoteAPI + '/api/v1/login/doLogin', bodyString, this.options)
      .map(res => res.json())
      .do(res => {
        if (res.success) {
          sessionStorage.setItem('auth_token', res.auth_token);
          sessionStorage.setItem('userName', res.userName);
        }
        return res.success;
    });
  }

}
