import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms' ;
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './pages/test/test.component';
import { TestStringPipe } from './pipes/test-string.pipe';
import { HomeComponent } from './pages/home/home.component';
import { PublicZoneComponent } from './pages/public-zone/public-zone.component';
import { PrivateZoneComponent } from './pages/private-zone/private-zone.component';
import { LoginComponent } from './pages/login/login.component';
import { MemberComponent } from './pages/member/member.component';
import { IssueComponent } from './pages/issue/issue.component';
import { IssueEditComponent } from './pages/issue-edit/issue-edit.component';
import { IssueAttachComponent } from './pages/issue-attach/issue-attach.component';
import { ProjectComponent } from './pages/project/project.component';
import { ProjectEditComponent } from './pages/project-edit/project-edit.component';
import { PicComponent } from './pages/pic/pic.component';
import { PicEditComponent } from './pages/pic-edit/pic-edit.component';
import { UserService } from './shared/user/user.service';
import { ProjectService } from './shared/project/project.service';
import { LoginGuard } from './shared/user/login.guard';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    TestStringPipe,
    HomeComponent,
    PublicZoneComponent,
    PrivateZoneComponent,
    LoginComponent,
    MemberComponent,
    IssueComponent,
    IssueEditComponent,
    IssueAttachComponent,
    ProjectComponent,
    ProjectEditComponent,
    PicComponent,
    PicEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [
    UserService,
    LoginGuard,
    ProjectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
