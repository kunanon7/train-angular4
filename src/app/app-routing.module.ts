import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicZoneComponent } from './pages/public-zone/public-zone.component';
import { LoginComponent } from './pages/login/login.component';

import { PrivateZoneComponent } from './pages/private-zone/private-zone.component';
import { MemberComponent } from './pages/member/member.component';
import { IssueComponent } from './pages/issue/issue.component';
import { IssueEditComponent } from './pages/issue-edit/issue-edit.component';
import { IssueAttachComponent } from './pages/issue-attach/issue-attach.component';
import { ProjectComponent } from './pages/project/project.component';
import { ProjectEditComponent } from './pages/project-edit/project-edit.component';
import { PicComponent } from './pages/pic/pic.component';
import { PicEditComponent } from './pages/pic-edit/pic-edit.component';
import { LoginGuard } from './shared/user/login.guard';

const routes: Routes = [
  {
    path: '',
    component: PublicZoneComponent,
    children: [{
      path: '',
      component: HomeComponent
    }, {
      path: 'login',
      component: LoginComponent
    }]
  } ,
  {
    path: 'main',
    component: PrivateZoneComponent,
    canActivate: [ LoginGuard ],
    children: [{
      path: 'member',
      component: MemberComponent
    }, {
      path: 'issue',
      component: IssueComponent
    }, {
      path: 'issueEdit',
      component: IssueEditComponent
    }, {
      path: 'issueAttach',
      component: IssueAttachComponent
    }, {
      path: 'project',
      component: ProjectComponent
    }, {
      path: 'projectEdit',
      component: ProjectEditComponent
    }, {
      path: 'pic',
      component: PicComponent
    }, {
      path: 'picEdit',
      component: PicEditComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
