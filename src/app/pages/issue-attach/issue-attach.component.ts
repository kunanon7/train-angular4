import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-issue-attach',
  templateUrl: './issue-attach.component.html',
  styleUrls: ['./issue-attach.component.css']
})
export class IssueAttachComponent implements OnInit {

  datas = [
    {
      issuePic : '01.jpg'
    } ,
    {
      issuePic : '02.jpg'
    },
    {
      issuePic : '03.jpg'
    },
    {
      issuePic : '04.jpg'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
