import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  userName = sessionStorage.getItem('userName');

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  onLogout() {
    sessionStorage.removeItem('auth_token');
    sessionStorage.removeItem('userName');
    this.router.navigate(['login']);
  }

}
