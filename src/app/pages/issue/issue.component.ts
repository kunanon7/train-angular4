import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {

  datas = [
    {
      issueCode : 'i001' ,
      issueDetail : 'detail 123' ,
      issuePic : '01.jpg',
      date : '11/12/2017' ,
      status : 'Open'
    } ,
    {
      issueCode : 'i002' ,
      issueDetail : 'detail 222' ,
      issuePic : '02.jpg',
      date : '10/12/2017' ,
      status : 'Close'
    },
    {
      issueCode : 'i003' ,
      issueDetail : 'detail 004' ,
      issuePic : '03.jpg',
      date : '12/12/2017' ,
      status : 'Open'
    },
    {
      issueCode : 'i004' ,
      issueDetail : 'detail i004' ,
      issuePic : '04.jpg',
      date : '13/12/2017' ,
      status : 'Open'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
