import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pic',
  templateUrl: './pic.component.html',
  styleUrls: ['./pic.component.css']
})
export class PicComponent implements OnInit {

  datas = [
    { picCode : '01', picName : 'pic01', picValue : 'pae' },
    { picCode : '02', picName : 'pic02', picValue : 'tiew' },
    { picCode : '03', picName : 'pic03', picValue : 'non' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
