import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../shared/project/project.service';
import { Project } from '../../shared/project/project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  project: Project;

  /*
  datas = [
    { projectCode : 'CHECKER01', projectName : 'CHECKER01', projectImage : 'CHECKER01.jpg', active : 'Y' },
    { projectCode : 'CHECKER02', projectName : 'CHECKER02', projectImage : 'CHECKER02.jpg', active : 'N' },
    { projectCode : 'CHECKER03', projectName : 'CHECKER03', projectImage : 'CHECKER03.jpg', active : 'N' }
  ];
  */
  datas;

  constructor(private projectService: ProjectService) {
    this.project = new Project();
  }

  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.projectService.loadItem(this.project).subscribe(
      res => {
        this.datas = res;
      },
      err => {console.log(err); }
    );
  }

}
