import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  showTitle = true;
  boolDis = true;
  xTest = '';
  datas = [1, 2, 3, 4];
  numTest = 10000.8456 ;
  dateNow = new Date();

  @Input()
  txtTitle;

  constructor() { }

  ngOnInit() {
  }

  gg(): void {
    this.showTitle = !this.showTitle;
  }

  gg2(): void {
    this.boolDis = !this.boolDis;
  }

  gg3(): void {
    this.datas.push(this.datas.length + 1);
  }

  ggDelete(idx): void {
    this.datas.splice(idx , 1);
  }
}
